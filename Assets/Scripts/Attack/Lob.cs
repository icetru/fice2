﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Lob : MonoBehaviour
{
    public Rigidbody bulletPrefabs;
    public Transform shootPoint;
    public GameObject cursor;
    public LayerMask layer;

    public bool canShoot = false;

    ControlsManager controlsManager;

    [SerializeField]
    int PlayerID;

    private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        controlsManager = FindObjectOfType<ControlsManager>();
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(controlsManager.GetKey(PlayerID, ControlKeys.Viser)))
        {
            canShoot = true;
        }

        if (Input.GetKeyDown(controlsManager.GetKey(PlayerID, ControlKeys.Quitter)))
        {
            canShoot = false;
        }

        if(canShoot == true)
        {
            TirEnLob();
        }
        else
        {
            cursor.SetActive(false);
        }
    }

    void TirEnLob()
    {
        Ray camRay = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(camRay, out hit, 100f, layer))
        {
            cursor.SetActive(true);
            cursor.transform.position = hit.point + Vector3.up * 0.1f;

            Vector3 Vo = calculateVelocity(hit.point, shootPoint.position, 1f);

            transform.rotation = Quaternion.LookRotation(Vo);

            if (Input.GetMouseButtonDown(0))
            {
                Rigidbody obj = Instantiate(bulletPrefabs, shootPoint.position, Quaternion.identity);
                obj.velocity = Vo;
            }
        }
        else
        {
            cursor.SetActive(false);
        }
    }
    Vector3 calculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        //Définir la distance x et y en premier
        Vector3 distance = target - origin;
        Vector3 distanceXZ = distance;
        distanceXZ.y = 0f;

        //création d'une variable qui représente la distance rechercher
        float Sy = distance.y;
        float Sxz = distanceXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distanceXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }
}
