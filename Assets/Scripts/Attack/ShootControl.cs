﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootControl : MonoBehaviour
{
    public bool isShooting;

    public Fire_Ice_Bullet bullet;

    public float bulletSpeed;

    public float Cooldown;

    public float ShootCounter;

    public Transform ShootPoint;

    public int bulletType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isShooting)
        {
            ShootCounter -= Time.deltaTime;
            if(ShootCounter <= 0)
            {
                ShootCounter = Cooldown;
                Fire_Ice_Bullet newbullet = Instantiate(bullet, ShootPoint.position, ShootPoint.rotation) as Fire_Ice_Bullet;
                newbullet.Speed = bulletSpeed;
                if(bulletType == 0)
                {
                    newbullet.tag = "feu";
                }
                if (bulletType == 1)
                {
                    newbullet.tag = "eau";
                }
            }
            isShooting = false;
        }
        else
        {
            ShootCounter = 0;
        }
    }
}
