﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Tir : MonoBehaviour
{
    //Variables Script Tir
    public Rigidbody bulletPrefabs;
    public Transform ShootPoint;
    public GameObject cursor;

    public GameObject icePlayer;
    public GameObject firePlayer;

    public bool canShoot = false;

    ControlsManager controlsManager;

    [SerializeField]
    int PlayerID;

    public ShootControl shootControl;
    public PlayerController movementFire;
    public PlayerController movementIce;
    private Camera cam;

    //////////////////////////////////////
    // Variables Shoot Control
    public bool isShooting;

    public Fire_Ice_Bullet bullet;

    public float bulletSpeed;

    public float Cooldown;

    public float ShootCounter;

    public int bulletType;

    ////////////////////////////


    // Start is called before the first frame update
    void Start()
    {
        controlsManager = FindObjectOfType<ControlsManager>();
        cam = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Viser2)))
        {
            if (PlayerID == 0)
            {
                if (this.gameObject.GetComponent<StatusFire>().Statut == StatusFire.State.Incandescent)
                {
                    movementFire.speed = 0.01f;
                    movementFire.speedRotation = 1;
                    firePlayer.transform.GetChild(2).gameObject.SetActive(true);
                }
            }
            if (PlayerID == 1)
            {
                if (this.gameObject.GetComponent<StatusIce>().Statut == StatusIce.State.Liquide || this.gameObject.GetComponent<StatusIce>().Statut == StatusIce.State.Glacon)
                {
                    movementIce.speed = 0.1f;
                    movementIce.speedRotation = 1;
                    icePlayer.transform.GetChild(2).gameObject.SetActive(true);
                }
            }

        }

        if (Input.GetKeyUp(controlsManager.GetKey(PlayerID, ControlKeys.Viser2)))
        {
            if (PlayerID == 0)
            {
                movementFire.speed = 3;
                movementFire.speedRotation = 10;
                firePlayer.transform.GetChild(2).gameObject.SetActive(false);
            }
            if (PlayerID == 1)
            {
                movementIce.speed = 3;
                movementIce.speedRotation = 10;
                icePlayer.transform.GetChild(2).gameObject.SetActive(false);
            }

            ShootCounter = Cooldown;
            Fire_Ice_Bullet newbullet = Instantiate(bullet, ShootPoint.position, ShootPoint.rotation) as Fire_Ice_Bullet;
            newbullet.Speed = bulletSpeed;
            if (PlayerID == 0)
            {
                newbullet.tag = "feu";
            }
            if (PlayerID == 1)
            {
                newbullet.tag = "eau";
            }

        }
    }
}

