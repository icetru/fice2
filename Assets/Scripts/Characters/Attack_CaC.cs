﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Attack_CaC : MonoBehaviour
{
    public GameObject Arm;
    Animator anim;

    ControlsManager controlsManager;

    [SerializeField]
    public int PlayerID;

    // Start is called before the first frame update
    void Start()
    {
        Arm.SetActive(false);
        anim = GetComponent<Animator>();
        controlsManager = FindObjectOfType<ControlsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(controlsManager.GetKey(PlayerID, ControlKeys.Tirer_Attaquer)))
        {
            if (PlayerID == 0)
            {
                if (this.gameObject.GetComponent<StatusFire>().Statut == StatusFire.State.Incandescent || this.gameObject.GetComponent<StatusFire>().Statut == StatusFire.State.Eteint)
                {
                    StartCoroutine(Attack());
                }
            }
            if (PlayerID == 1)
            {
                if (this.gameObject.GetComponent<StatusIce>().Statut == StatusIce.State.Liquide)
                {
                    StartCoroutine(Attack());
                }
            }
            
        }
    }

    IEnumerator Attack()
    {
        if (PlayerID == 0)
        {
            if (this.gameObject.GetComponent<StatusFire>().Statut == StatusFire.State.Incandescent)
            {
                Arm.tag = "feu";
            }
            if (this.gameObject.GetComponent<StatusFire>().Statut == StatusFire.State.Eteint)
            {
                Arm.tag = "";
            } 
        }
        if (PlayerID == 1)
        {
            //if (this.gameObject.GetComponent<StatusIce>().Statut == StatusIce.State.Base)
            //{
            //    Arm.tag = "glace";
            //}
            if (this.gameObject.GetComponent<StatusIce>().Statut == StatusIce.State.Liquide)
            {
                Arm.tag = "eau";
            }
        }
        Arm.SetActive(true);
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(0.8f);
        Arm.SetActive(false);
    }
}
