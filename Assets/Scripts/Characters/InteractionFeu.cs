﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionFeu : MonoBehaviour
{

    public StatusFire statutChange;
    public bool carrying;
    public Transform hand;
    public Sprite[] sprInteraction;
    public GameObject goImgInterac;

   
    int layerMask = ~(9);

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Quaternion rotation = Quaternion.AngleAxis(30.0f, transform.right);
        
        Debug.DrawRay(transform.position, rotation * this.transform.forward * 2f, Color.red);
        if (Physics.SphereCast(transform.position, 0.05f,rotation * this.transform.forward, out hit, 2f))
        {
            //UI affichage touche interaction 
            if (hit.transform.tag == "SFire" && statutChange.Statut == StatusFire.State.Incandescent || hit.transform.name == "fbx_clef" || hit.transform.tag == "climbable")
            {
                goImgInterac.SetActive(true);
                goImgInterac.gameObject.GetComponent<Image>().sprite = sprInteraction[1];
            }
            else
            {
                goImgInterac.SetActive(false);
            }

            if (!carrying)
            {

                if (hit.collider.name == "fbx_clef")
                {
                    //ramasserClé/Objet
                    hit.transform.parent = hand.transform;
                    hand.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1, this.gameObject.transform.position.z);
                    StartCoroutine(Wait());
                }
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!carrying)
                {

                    if (hit.collider.tag == "climbable")
                    {
                        // Escalade 1 bloc de haut
                        this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 3, this.gameObject.transform.position.z);
                        this.gameObject.transform.Translate(Vector3.forward * 2, Space.Self);
                    }

                }
            }
        }

        if (Physics.SphereCast(transform.position, 0.05f, rotation * this.transform.forward, out hit, 5, layerMask))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.collider.tag == "SFire")
                {
                    //Changement statut environnement / Source de pouvoir
                    statutChange.explosion();
                }

            }
            Debug.Log(hit.collider.gameObject.tag);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (carrying)
            {
                //dropClé
                Transform tr = hand.transform.Find("fbx_clef");
                tr.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y - 1, this.gameObject.transform.position.z);
                tr.transform.parent = null;
                carrying = false;
            }
        }
         
    }



    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.5f); carrying = true;
    }
}
