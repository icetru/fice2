﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractionGlace : MonoBehaviour
{
    public StatusIce statutChange;
    public bool carrying;
    public Transform hand;

    public Sprite[] sprInteraction;
    public GameObject goImgInterac;


    // Update is called once per frame
    void Update()
    {

        RaycastHit hit;
        Quaternion rotation = Quaternion.AngleAxis(30.0f, transform.right);
        Debug.DrawRay(transform.position, rotation * this.transform.forward * 2, Color.red);
        if (Physics.SphereCast(transform.position,0.05f, rotation * this.transform.forward, out hit, 2))
        {
            //UI touche interaction
            //if (hit.transform.gameObject.name == "fbx_clef" || hit.transform.gameObject.tag == "climbable")
            //{
            //    goImgInterac.SetActive(true);
            //    goImgInterac.gameObject.GetComponent<Image>().sprite = sprInteraction[1];
            //}
            //else
            //{
            //    goImgInterac.SetActive(false);
            //}

            if (!carrying)
            {

                if (hit.collider.name == "fbx_clef")
                {
                    //ramasserClé/Objet
                    hit.transform.parent = hand.transform;
                    hand.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1, this.gameObject.transform.position.z);
                    StartCoroutine(Wait());
                }
            }

            if (Input.GetKeyDown(KeyCode.Keypad4))
            {

                if (hit.transform.gameObject.tag == "climbable")
                {
                    //Escalade 1 bloc de haut
                    this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 3, this.gameObject.transform.position.z);
                    this.gameObject.transform.Translate(Vector3.forward * 2, Space.Self);
                }
                if (hit.transform.gameObject.tag == "blizzardA")
                {
                    //Changement Statut environnement / Source de pouvoir
                    statutChange.glacon();
                }

            }

        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            if (carrying)
            {
                //dropClé
                Transform tr = hand.transform.Find("fbx_clef");
                tr.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y-1, this.gameObject.transform.position.z);
                tr.transform.parent = null;
                carrying = false;
            }
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.5f); carrying = true;
    }
}
