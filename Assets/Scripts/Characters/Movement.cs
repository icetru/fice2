﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody rb;
    Transform cam;

    [Range(0,10)]
    public float Speed;

    [Range(0, 10)]
    public float RotationSpeed;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform;
    }

    void Update()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 playerMovement = new Vector3(horizontal, 0f, vertical) * Speed * Time.deltaTime;

        playerMovement = new Vector3(playerMovement.x, 0f, playerMovement.z);

        rb.MovePosition(rb.position + playerMovement);

        if(playerMovement != Vector3.zero)
        {
            Quaternion direction = Quaternion.LookRotation(playerMovement);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, RotationSpeed);
        }
    }
}
