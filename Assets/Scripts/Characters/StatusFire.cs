﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusFire : MonoBehaviour
{
    public enum State
    {
        //Base,
        Eteint,
        Explosif,
        Incandescent,
    }

    private float radius = 10F;
    private float power = 25F;

    private int heat;

    public StatusIce otherPlayer;

    public State Statut;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SwitchIncandescent());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            StopAllCoroutines();
            StartCoroutine(SwitchEteint());
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StopAllCoroutines();
            StartCoroutine(SwitchExplosif());
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StopAllCoroutines();
            StartCoroutine(SwitchIncandescent());
        }

        if (Statut == StatusFire.State.Explosif)
        {
            //Explosion prématuré
            if (Input.GetKeyDown(KeyCode.E))
            {
                StopAllCoroutines();
                StartCoroutine(Explode());
            }
        }

        if (Statut == StatusFire.State.Eteint)
        {
            //Interaction rallumage statut feu
            if (Input.GetKeyDown(KeyCode.E))
            {
                heat++;
            }
        }

        if (heat > 0)
        {
            //Reset rallumage feu
            StartCoroutine(heatReset());
        }

        if (heat >= 3)
        {
            //Déclenchement rallumage feu
            StartCoroutine(SwitchIncandescent());
        }

    }
    private void OnCollisionEnter(Collision element)
    {
        if (Statut == State.Incandescent)
        {
            //Switch statut éteint sur collision eau
            if (element.gameObject.tag == "eau")
            {
                StopAllCoroutines();
                StartCoroutine(SwitchEteint());
            }
        }
    }
    public void incandescent()
    {
        //changement forme feu
        StopAllCoroutines();
        StartCoroutine(SwitchIncandescent());
    }

    public void explosion()
    {
        //changement forme explosif
        StartCoroutine(SwitchExplosif());
    }

    private void OnTriggerEnter(Collider element)
    {
        if (Statut == StatusFire.State.Incandescent)
        {
            if (element.transform.tag == "enflame")
            {
                //Activation "torche" 
                element.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.yellow);
                element.GetComponent<Animator>().SetBool("inFire", true);
                element.transform.gameObject.tag = "inFire";
            }
        }

        if (element.gameObject.tag == "Respawn")
        {
            //Respawn si chute 
            this.gameObject.transform.position = new Vector3(-9, 3, 1);
        }
    }

    //private void OnTriggerStay(Collider element)
    //{
    //    if (Statut == StatusFire.State.Incandescent && element.gameObject.layer == 10)
    //    {
    //        Destroy(element.transform.gameObject);
    //    }
    //}



    IEnumerator SwitchEteint()
    {
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        Statut = State.Eteint;
        this.gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.black);
        yield return null;
    }

    IEnumerator SwitchExplosif()
    {
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        this.gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.magenta);
        yield return new WaitForSeconds(0.5f);
        Statut = State.Explosif;

        yield return new WaitForSeconds(5f);
        StartCoroutine(Explode());
    }

    IEnumerator Explode()
    {
        //Explosion du joueur, retourne en forme feu après l'explosion
        this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit2 in colliders)
        {
            if (hit2.transform.name == "IcePlayer")
            {
                hit2.gameObject.GetComponent<CharacterController>().enabled = false;
                Rigidbody rb = hit2.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(power, explosionPos, radius, 1.5F, ForceMode.Impulse);
                yield return new WaitForSeconds(0.75f);
                hit2.gameObject.GetComponent<CharacterController>().enabled = true;
            }

        }
        yield return new WaitForSeconds(0.75f);

        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        StartCoroutine(SwitchIncandescent());
    }

    IEnumerator SwitchIncandescent()
    {
        this.gameObject.transform.GetChild(1).gameObject.SetActive(true);
        Statut = State.Incandescent;
        this.gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
        yield return null;
    }

    IEnumerator heatReset()
    {
        yield return new WaitForSeconds(1.0f);
        heat = 0;
    }
}
