﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusIce : MonoBehaviour
{
    public bool inWater;
    Coroutine pushed;
    public enum State
    {
        Liquide,
        Vapeur,
        Glacon,
    }

    public State Statut;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SwitchLiquide());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            StopAllCoroutines();
            StartCoroutine(SwitchLiquide());
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            StopAllCoroutines();
            StartCoroutine(SwitchVapeur());
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            StopAllCoroutines();
            StartCoroutine(SwitchGlacon());
        }

        if (Statut == State.Liquide)
        {
            //Ignore collision pour forme liquide, permet de traverser l'eau et de s'y déplacer
            Physics.IgnoreLayerCollision(11, 4);
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            //Saut Pschitt, action forme vapeur
            if (Statut == StatusIce.State.Vapeur)
            {
                StartCoroutine(ActionVapeur());
            }

        }

        if (inWater)
        {
            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                Debug.Log("saumon");
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 3, this.gameObject.transform.position.z);
                this.gameObject.transform.Translate(Vector3.forward * 2, Space.Self);
            }
        }
    }
    private void OnCollisionEnter(Collision element)
    {
        if (Statut == State.Liquide)
        {
            if (element.gameObject.tag == "feu")
            {
                StopAllCoroutines();
                StartCoroutine(SwitchVapeur());
            }
        }

        if (Statut == State.Glacon)
        {
            if (element.gameObject.tag == "SFire" || element.gameObject.tag == "feu")
            {
                StopAllCoroutines();
                StartCoroutine(SwitchLiquide());
            }
        }

        if (element.gameObject.tag == "Respawn")
        {
            this.gameObject.transform.position = new Vector3(-9, 3, 2);
        }
    }

    private void OnTriggerEnter(Collider element)
    {
        if (Statut == State.Glacon)
        {
            //Si collision avec feu -> changement forme liquide
            if (element.gameObject.tag == "SFire")
            {
                StopAllCoroutines();
                StartCoroutine(SwitchLiquide());
            }

            //Activation "torche" élémentaire du joueur glace
            if (element.gameObject.tag == "freeze")
            {
                element.GetComponent<Renderer>().material.SetColor("_BaseMap", Color.yellow);
                element.GetComponent<Animator>().SetBool("inFire", true);
                element.transform.gameObject.tag = "inFire";
            }
        }
        if (Statut == State.Liquide)
        {
            //Activation "torche" élémentaire du joueur glace
            if (element.gameObject.tag == "freeze")
            {
                element.GetComponent<Renderer>().material.SetColor("_BaseMap", Color.yellow);
                element.GetComponent<Animator>().SetBool("inFire", true);
                element.transform.gameObject.tag = "inFire";
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.tag);
        if (other.gameObject.tag == "eau")
        {
            inWater = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "eau")
        {
            inWater = false;
        }
    }


    public void liquide()
    {
        //changement forme liquide
        StopAllCoroutines();
        StartCoroutine(SwitchLiquide());
    }

    public void vapeur()
    {
        //changement forme vapeur
        StopAllCoroutines();
        StartCoroutine(SwitchVapeur());
    }

    public void glacon()
    {
        //changement forme glacon
        StopAllCoroutines();
        StartCoroutine(SwitchGlacon());
    }

    public void ignoreCollision()
    {
        StartCoroutine(noColli());
    }


    //Coroutine / Statuts Change


    IEnumerator SwitchLiquide()
    {
        yield return new WaitForSeconds(0.01f);
        Statut = State.Liquide;
        //this.gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.cyan);
        this.gameObject.GetComponent<Collider>().enabled = true;
        this.gameObject.GetComponent<PlayerController>().gravity = 10;
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        yield return null;
    }

    IEnumerator SwitchVapeur()
    {
        Statut = State.Vapeur;
        //this.gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.grey);
        this.gameObject.GetComponent<Collider>().enabled = false;
        this.gameObject.GetComponent<PlayerController>().gravity = 0;
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        StartCoroutine(SwitchLiquide());
    }

    IEnumerator SwitchGlacon()
    {
        Statut = State.Glacon;
        Physics.IgnoreLayerCollision(11, 4, false);
        //this.gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
        this.gameObject.GetComponent<Collider>().enabled = true;
        this.gameObject.GetComponent<PlayerController>().gravity = 10;
        this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        this.gameObject.transform.GetChild(1).gameObject.SetActive(true);
        //this.gameObject.transform.GetComponent<PlayerMovement>().enabled = false;
        yield return null;
    }

    IEnumerator noColli()
    {
        GameObject player = GameObject.Find("IcePlayer");
        Physics.IgnoreLayerCollision(11, 4);
        //Collider[] collision = GetComponent<Collider>();
        //Physics.IgnoreCollision(player.GetComponent<Collider>(),)
        yield return new WaitForSeconds(1f);
        Physics.IgnoreLayerCollision(11, 4, false);
    }

    //Saut Pschitt, retour forme liquide après utilisation
    IEnumerator ActionVapeur()
    {
        Vector3 currentPos = this.gameObject.transform.localPosition;
        Vector3 posToReach = this.gameObject.transform.localPosition + new Vector3(0, 5, 0);
        var time = 0.60f;
        var t = 0f;

        while (t < time)
        {
            t += Time.deltaTime / time;
            this.gameObject.transform.localPosition = (currentPos = Vector3.Lerp(currentPos, posToReach, t));
            yield return null;
        }
        yield return null;
        StartCoroutine(SwitchLiquide());
    }
}
