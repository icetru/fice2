﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gel : MonoBehaviour
{

    public bool iceable = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider element)
    {
        if (element.gameObject.tag == "blizzardA")
        {
            if (iceable)
            {
                this.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider element)
    {
        if (element.gameObject.tag == "blizzardA")
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
