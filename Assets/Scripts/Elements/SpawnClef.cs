﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnClef : MonoBehaviour
{
    public GameObject clef;
    public GameObject Torche1;
    public GameObject Torche2;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Torche1.GetComponent<Animator>().GetBool("inFire") == true && Torche2.GetComponent<Animator>().GetBool("inFire"))
        {
            clef.SetActive(true);
        }
    }
}
