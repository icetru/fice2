﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructurePass : MonoBehaviour
{
    public int passNumber = 3;
    public GameObject etatFeu;
    public GameObject etatGlace;
    // Start is called before the first frame update
    void Start()
    {
        etatFeu = GameObject.Find("FirePlayer");
        etatGlace = GameObject.Find("IcePlayer");
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.GetChild(0).gameObject.activeInHierarchy == true)
        {
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
        if(passNumber <= 0)
        {
            Debug.Log("Fondu");
            passNumber = 3;
            this.transform.GetChild(1).gameObject.SetActive(true);
            this.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    private void OnCollisionExit(Collision element)
    {
        if (this.transform.GetChild(0).gameObject.activeInHierarchy == true)
        {
            if (element.gameObject == etatFeu)
            {
                //if (etatFeu.GetComponent<StatusFire>().Statut == StatusFire.State.Base)
                //{
                //    passNumber = 3;
                //    this.transform.GetChild(0).gameObject.SetActive(false);
                //    this.transform.GetChild(1).gameObject.SetActive(true);
                //}

                if (etatFeu.GetComponent<StatusFire>().Statut == StatusFire.State.Eteint)
                {
                    passNumber--;
                }
            }

            if (element.gameObject == etatGlace)
            {
                if (etatGlace.GetComponent<StatusIce>().Statut == StatusIce.State.Liquide || etatGlace.GetComponent<StatusIce>().Statut == StatusIce.State.Glacon)
                {
                    passNumber--;
                }
            }
        }
    }

    private void OnTriggerStay(Collider element)
    {
        if (element.transform.tag == "SFire")
        {
            passNumber = 3;
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider element)
    {
        if (element.transform.tag == "SFire")
        {
            passNumber = 3;
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider element)
    {
        if (element.transform.tag == "blizzardA")
        {
            passNumber = 3;
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(true);
        }
    }
}
