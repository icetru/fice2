﻿namespace Enums
{
    //we can list all the controls that the game would contain.
    public enum ControlKeys{

        Avancer,

        Reculer,

        Droite,

        Gauche,

        Viser,

        Viser2,

        Quitter,

        Tirer_Attaquer,

        Action_de_statut,

        Action_contextuelle,
    };
}