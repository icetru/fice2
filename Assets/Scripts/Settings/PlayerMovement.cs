﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rb;

    [SerializeField]
    public float RotationSpeed;

    [SerializeField]
    int PlayerID;

    [SerializeField]
    public float Speed;

    ControlsManager controlsManager;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controlsManager = FindObjectOfType<ControlsManager>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float Vertical = Input.GetAxisRaw("Vertical");

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Avancer)))
        {
            Vector3 playerMovement = (Vector3.forward * Speed * Time.deltaTime);
            playerMovement = new Vector3(playerMovement.x, 0f, playerMovement.z);

            rb.MovePosition(rb.position + playerMovement);

            if (playerMovement != Vector3.zero)
            {
                this.GetComponent<Rigidbody>().AddForce(playerMovement);
                Quaternion direction = Quaternion.LookRotation(playerMovement);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, RotationSpeed);
            }
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Reculer)))
        {
            Vector3 playerMovement = (Vector3.back * Speed * Time.deltaTime);
            playerMovement = new Vector3(playerMovement.x, 0f, playerMovement.z);

            rb.MovePosition(rb.position + playerMovement);

            if (playerMovement != Vector3.zero)
            {
                this.GetComponent<Rigidbody>().AddForce(playerMovement);
                Quaternion direction = Quaternion.LookRotation(playerMovement);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, RotationSpeed);
            }
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Droite)))
        {
            Vector3 playerMovement = (Vector3.right * Speed * Time.deltaTime);
            playerMovement = new Vector3(playerMovement.x, 0f, playerMovement.z);

            rb.MovePosition(rb.position + playerMovement);

            if (playerMovement != Vector3.zero)
            {
                this.GetComponent<Rigidbody>().AddForce(playerMovement);
                Quaternion direction = Quaternion.LookRotation(playerMovement);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, RotationSpeed);
            }
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Gauche)))
        {
            Vector3 playerMovement = (Vector3.left * Speed * Time.deltaTime);
            playerMovement = new Vector3(playerMovement.x, 0f, playerMovement.z);

            rb.MovePosition(rb.position + playerMovement);

            if (playerMovement != Vector3.zero)
            {
                this.GetComponent<Rigidbody>().AddForce(playerMovement);
                Quaternion direction = Quaternion.LookRotation(playerMovement);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, RotationSpeed);
            }
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Tirer_Attaquer)))
        {
            Debug.Log( "" + PlayerID + " Action Fired");
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Viser)))
        {
            Debug.Log("" + PlayerID + " Action target");
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Action_de_statut)))
        {
            Debug.Log("" + PlayerID + " Action ice/fire");
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Action_contextuelle)))
        {
            Debug.Log("" + PlayerID + " Action contexte");
        }
    }
}
