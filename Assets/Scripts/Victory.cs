﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : MonoBehaviour
{
    public bool player1;
    public bool player2;
    public bool key;
    public GameObject poceblo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(key && player2 && player1)
        {
            poceblo.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "fbx_clef")
        {
            key = true;
        }
        if (other.name == "IcePlayer")
        {
            player2 = true;
        }
        if (other.name == "FirePlayer")
        {
            player1 = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "fbx_clef")
        {
            key = false;
        }
        if (other.name == "IcePlayer")
        {
            player2 = false;
        }
        if (other.name == "FirePlayer")
        {
            player1 = false;
        }
    }
}
