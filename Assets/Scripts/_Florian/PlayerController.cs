﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class PlayerController : MonoBehaviour
{
    [Tooltip("ID du joueur: 0 = fire, 1 = water"),Header("Identifiant du joueur")]
    public int playerID = 0;

    [Tooltip("La gravité subit par les joueurs"), Header("Gravitée")]
    public float gravity = 9.81f;
    //Le calcule de gravité
    private float gCalculate = 0;

    //Les inputs horizontaux et verticaux
    private float horizontalInput = 0;
    private float verticalInput = 0;

    [Header("Inertie")]
    [Tooltip("Inertie horizontale pour l'accélération")]
    public float horizontalInertieIn = 4f;
    [Tooltip("Inertie verticale pour l'accélération")]
    public float verticalInertieIn = 4f;
    [Tooltip("Inertie horizontale pour le ralentissement")]
    public float horizontalInertieOut = 5f;
    [Tooltip("Inertie verticale pour le ralentissement")]
    public float verticalInertieOut = 5f;
    [Tooltip("Inertie de la rotation à l'accélération")]
    public float rotationInternieIn = 5;
    [Tooltip("Inertie de la rotation pour le ralentissement")]
    public float rotationInternieOut = 7;
    //Pour l'influence de la rotation une fois que l'on arrête d'appuyer sur les touches de déplacements
    private float rotationModifier = 0;

    [Header("Vitesse")]
    [Tooltip("La vitesse du joueur")]
    public float speed = 5;
    [Tooltip("Vitesse de rotation du personnage")]
    public float speedRotation = 5;

    //Le characterController
    private CharacterController cc;
    private ControlsManager cm;

    void Start()
    {
        //On récupère le characterController
        cc = GetComponent<CharacterController>();
        cm = FindObjectOfType(typeof(ControlsManager)) as ControlsManager;
    }

    void Update()
    {
        //Le deltaTime;
        float dt = Time.deltaTime;

        //Initialisation du vecteur de mouvement
        Vector3 motion = Vector3.zero;

        //Calcul de la gravité
        CalculateGravity(dt);

        //Assignation des variables pour bouger dans différentes directions (calcul des inputs)
        AssignInputsVar(dt);

        //application du déplacement vertical
        motion.x = horizontalInput * speed;
        //application de la gravité au joueur
        motion.y = gCalculate;
        //application du déplacement horizontal
        motion.z = verticalInput* speed;

        //Multiplication par le deltaTime
        motion *= dt;

        //Déplacement du joueur selon le vecteur motion
        cc.Move(motion);

        //Rotate le joueur
        RotatePlayer(dt,new Vector3(motion.x,0,motion.z));
    }

    void CalculateGravity(float dt)
    {
        //Si le joueur est sur le sol
        if (cc.isGrounded)
        {
            //La gravity revient à celle de base
            gCalculate = -gravity;
        }
        else
        {
            //La gravité devient de plus en plus fort
            gCalculate -= gravity * dt;
        }
    }

    //Tourne le joueur
    void RotatePlayer(float dt,Vector3 movementVector)
    {
        //Si une touche de déplacement est appuyé
        if (Input.GetKey(cm.GetKey(playerID, ControlKeys.Avancer)) || Input.GetKey(cm.GetKey(playerID, ControlKeys.Reculer)) || Input.GetKey(cm.GetKey(playerID, ControlKeys.Gauche)) || Input.GetKey(cm.GetKey(playerID, ControlKeys.Droite)))
        {
            //On monte progressivement la variable rotationModifier vers 1
            rotationModifier = Mathf.Lerp(rotationModifier,1,rotationInternieIn * dt);
        }
        //Si aucune touche de déplacement n'est appuyé
        else
        {
            //La variable rotationModifier descend vers 0
            rotationModifier = Mathf.Lerp(rotationModifier, 0, rotationInternieOut * dt);

            if (rotationModifier <= 0.01f && rotationModifier >= -0.01f)
                rotationModifier = 0;
        }

        //Update de la rotation en permanace vers le vecteur de déplacement
        Quaternion direction = Quaternion.LookRotation(movementVector);
        //Rotation en fonction de la vitesse, la vitesse est changé par la rotationModifier qui permet l'inertie
        transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, speedRotation * rotationModifier);
    }

    //Calcule les inputs
    void AssignInputsVar(float dt)
    {
        //Input pour avancer
        if (Input.GetKey(cm.GetKey(playerID, ControlKeys.Avancer)))
        {
            //Si la valeur est inférieure à 0 elle devient 0
            if (verticalInput < 0)
                verticalInput = 0;

            //L'input augmente tant que la touche est pressée
            verticalInput = Mathf.Lerp(verticalInput, 1, verticalInertieIn * dt);
        }

        //Input pour reculer
        if (Input.GetKey(cm.GetKey(playerID, ControlKeys.Reculer)))
        {
            //Si la valeur est supérieure à 0 elle devient 0
            if (verticalInput > 0)
                verticalInput = 0;

            //L'input diminue tant que la touche est pressée
            verticalInput = Mathf.Lerp(verticalInput, -1, verticalInertieIn * dt);
        }
        
        //Input pour aller à droite
        if (Input.GetKey(cm.GetKey(playerID, ControlKeys.Droite)))
        {
            //Si la valeur est inférieure à 0 elle devient 0
            if (horizontalInput < 0)
                horizontalInput = 0;

            //L'input augmente tant que la touche est pressée
            horizontalInput = Mathf.Lerp(horizontalInput, 1, horizontalInertieIn * dt);

        }

        //Input pour aller à gauche
        if (Input.GetKey(cm.GetKey(playerID, ControlKeys.Gauche)))
        {
            //Si la valeur est supérieure à 0 elle devient 0
            if (horizontalInput > 0)
                horizontalInput = 0;

            //L'input diminue tant que la touche est pressée
            horizontalInput = Mathf.Lerp(horizontalInput, -1, horizontalInertieIn * dt);
        }

        //S'il n'appuie sur aucune touche verticale
        if (!Input.GetKey(cm.GetKey(playerID, ControlKeys.Reculer)) && !Input.GetKey(cm.GetKey(playerID, ControlKeys.Avancer)))
        {
            verticalInput = Mathf.Lerp(verticalInput, 0, verticalInertieOut * dt);

            if (verticalInput <= 0.01f && verticalInput >= -0.01f)
                verticalInput = 0;
        }

        //S'il n'appuie sur aucune touche horizontale
        if (!Input.GetKey(cm.GetKey(playerID, ControlKeys.Gauche)) && !Input.GetKey(cm.GetKey(playerID, ControlKeys.Droite)))
        {
            horizontalInput = Mathf.Lerp(horizontalInput, 0, horizontalInertieOut * dt);

            if (horizontalInput <= 0.01f && horizontalInput >= -0.01f)
                horizontalInput = 0;
        }
    }
}
