﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetardementIce : MonoBehaviour
{
    Animator anim;

    public GameObject Bomb;
    [Range(0,10)]       
    public int BombCount = 1;

    // Start is called before the first frame update
    void Start()
    {
        BombCount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L) && BombCount == 1)
        {
            StartCoroutine(BombPose());
        }
    }

    IEnumerator BombPose()
    {
        Instantiate(Bomb, transform.position + (transform.forward * 1), Quaternion.identity);
        BombCount = 0;
        anim = GameObject.Find("Ice(Clone)").GetComponent<Animator>();
        yield return new WaitForSeconds(3);
        anim.SetTrigger("Bomb ready");
        yield return new WaitForSeconds(0.5f);
        Destroy(GameObject.Find("Ice(Clone)"));
        BombCount = 1;
    }
}
